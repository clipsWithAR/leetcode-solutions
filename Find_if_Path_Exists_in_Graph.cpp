class Solution {
public:
    bool validPath(int n, vector<vector<int>>& edges, int start, int end) {
        vector<vector<int>> adjList(n);
        stack<int> s;
        vector<bool> seen(n,false);
        int u,v,currVertex,secondVertex;
        for(int i=0; i<edges.size(); i++){
            u = edges[i][0];
            v = edges[i][1];
            adjList[u].push_back(v);
            adjList[v].push_back(u);
        }
        s.push(start);
        seen[start] = true;
        while(!s.empty()){
            currVertex = s.top();
            for(int i=0; i<adjList[currVertex].size(); i++){
                secondVertex = adjList[currVertex][i];
                if(!seen[secondVertex]){
                    s.push(secondVertex);
                    seen[secondVertex]=true;
                }
            }
            s.pop();
        }
        if(seen[end])
            return true;
        return false;   
    }
};